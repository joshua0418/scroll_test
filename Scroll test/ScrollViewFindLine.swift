//
//  ScrollViewFindLine.swift
//  Scroll test
//
//  Created by IUI on 2015/5/21.
//  Copyright (c) 2015年 IUI. All rights reserved.
//

import UIKit
import Foundation
import CoreData





extension UIScrollView {
    
    
    
    func initWithframes(frame: CGRect){
        self.opaque = false
        
        self.backgroundColor = UIColor.clearColor()
        // clearRect will cause a black square
        self.backgroundColor = self.backgroundColor!.colorWithAlphaComponent(1)
        // but uncomment the next line: clearRect will cause a clear square!
        self.backgroundColor = self.backgroundColor!.colorWithAlphaComponent(0.99)
        self.backgroundColor = UIColor.redColor()
        println("Layer opaque: \(self.layer.opaque)")
    }


    
    override public func drawRect(rect: CGRect)
    {
        var i = 0
        println("!!!\(self.backgroundColor)\(self.layer.opaque)")
   //     self.opaque = false
   //     self.backgroundColor = UIColor.clearColor()
        println("!!!\(self.backgroundColor)\(self.layer.opaque)")
       // self.backgroundColor = self.backgroundColor!.colorWithAlphaComponent(1)
        
        println("!!!\(self.backgroundColor)\(self.layer.opaque)")
        initWithframes(self.frame)
        println("!!!\(self.backgroundColor)\(self.layer.opaque)")
        
        
        let context = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(context, 3.0)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let components: [CGFloat] = [1.0, 0.0, 1.0, 1.0]
        let color = CGColorCreate(colorSpace, components)
        
    
        do{
            
            if(subviews[i].frame.origin.x == 0.0){
                println("Hello")
                
           /*     let context = UIGraphicsGetCurrentContext()
                CGContextSetLineWidth(context, 2.0)
                let colorSpace = CGColorSpaceCreateDeviceRGB()
                let components: [CGFloat] = [0.0, 0.0, 1.0, 1.0]
                let color = CGColorCreate(colorSpace, components)*/
            
                
                CGContextSetStrokeColorWithColor(context, color)
                CGContextMoveToPoint(context, self.subviews[i].frame.origin.x, self.subviews[i].frame.origin.y-50)
                CGContextAddLineToPoint(context, self.subviews[i].frame.origin.x, self.subviews[i].frame.origin.y + self.subviews[i].frame.height+50)
                CGContextStrokePath(context)
                
            }
            
            if(subviews[i].frame.origin.y == 0.0){
                println("Hello")
            /*    let context = UIGraphicsGetCurrentContext()
                CGContextSetLineWidth(context, 2.0)
                let colorSpace = CGColorSpaceCreateDeviceRGB()
                let components: [CGFloat] = [0.0, 0.0, 1.0, 1.0]
                let color = CGColorCreate(colorSpace, components)*/
                CGContextSetStrokeColorWithColor(context, color)
                CGContextMoveToPoint(context, self.subviews[i].frame.origin.x-50, self.subviews[i].frame.origin.y)
                CGContextAddLineToPoint(context, self.subviews[i].frame.origin.x+self.subviews[i].frame.width+50, self.subviews[i].frame.origin.y)
                CGContextStrokePath(context)
                
            }
            
            if((subviews[i].frame.origin.y+subviews[i].frame.height) == self.frame.height){
                println("Hello")
            /*    let context = UIGraphicsGetCurrentContext()
                CGContextSetLineWidth(context, 2.0)
                let colorSpace = CGColorSpaceCreateDeviceRGB()
                let components: [CGFloat] = [0.0, 0.0, 1.0, 1.0]
                let color = CGColorCreate(colorSpace, components)*/
                CGContextSetStrokeColorWithColor(context, color)
                CGContextMoveToPoint(context, self.subviews[i].frame.origin.x-50, self.subviews[i].frame.origin.y+subviews[i].frame.height)
                CGContextAddLineToPoint(context, self.subviews[i].frame.origin.x+self.subviews[i].frame.width+50, self.subviews[i].frame.origin.y+subviews[i].frame.height)
                CGContextStrokePath(context)
                
            }
            
            if((subviews[i].frame.origin.x+subviews[i].frame.width) == self.frame.width){
                println("Hello")
             /*   let context = UIGraphicsGetCurrentContext()
                CGContextSetLineWidth(context, 2.0)
                let colorSpace = CGColorSpaceCreateDeviceRGB()
                let components: [CGFloat] = [0.0, 0.0, 1.0, 1.0]
                let color = CGColorCreate(colorSpace, components)*/
                CGContextSetStrokeColorWithColor(context, color)
                CGContextMoveToPoint(context, subviews[i].frame.origin.x+subviews[i].frame.width, self.subviews[i].frame.origin.y-50)
                CGContextAddLineToPoint(context, subviews[i].frame.origin.x+subviews[i].frame.width, self.subviews[i].frame.origin.y+subviews[i].frame.height+50)
                CGContextStrokePath(context)
                
            }
            
            if((subviews[i].frame.origin.x+(subviews[i].frame.width/2)) == (self.frame.width/2)){
                println("Hello")
            /*    let context = UIGraphicsGetCurrentContext()
                CGContextSetLineWidth(context, 2.0)
                let colorSpace = CGColorSpaceCreateDeviceRGB()
                let components: [CGFloat] = [0.0, 0.0, 1.0, 1.0]
                let color = CGColorCreate(colorSpace, components)*/
                CGContextSetStrokeColorWithColor(context, color)
                CGContextMoveToPoint(context, subviews[i].frame.origin.x+(subviews[i].frame.width/2), self.subviews[i].frame.origin.y-50)
                CGContextAddLineToPoint(context, subviews[i].frame.origin.x+(subviews[i].frame.width/2), self.subviews[i].frame.origin.y+subviews[i].frame.height+50)
                CGContextStrokePath(context)
                
            }
            
            if((subviews[i].frame.origin.y+(subviews[i].frame.height/2)) == (self.frame.height/2)){
                println("Hello")
             /*   let context = UIGraphicsGetCurrentContext()
                CGContextSetLineWidth(context, 2.0)
                let colorSpace = CGColorSpaceCreateDeviceRGB()
                let components: [CGFloat] = [0.0, 0.0, 1.0, 1.0]
                let color = CGColorCreate(colorSpace, components)*/
                CGContextSetStrokeColorWithColor(context, color)
                CGContextMoveToPoint(context, subviews[i].frame.origin.x-50, (subviews[i].frame.origin.y+(subviews[i].frame.height/2)))
                CGContextAddLineToPoint(context, subviews[i].frame.origin.x+subviews[i].frame.width+50, (subviews[i].frame.origin.y+(subviews[i].frame.height/2)))
                CGContextStrokePath(context)
                
            }
            
            println("hi \(self.frame.height) \(self.frame.origin.y)")
            println("\(i) : \(subviews[i].frame.origin.x) \(subviews[i].frame.origin.y)")
            i++
        }while i<(subviews.count-2)
        
       /* if(i==subviews.count-2){
        initWithframes(self.frame)
        println("!!!\(self.frame.height)\(self.layer.opaque)")
        }*/

        println("!!!\(self.backgroundColor)\(self.layer.opaque)")
        
    }
    
    
}

