//
//  ViewController.swift
//  Scroll test
//
//  Created by IUI on 2015/5/21.
//  Copyright (c) 2015年 IUI. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class ViewController: UIViewController, UIScrollViewDelegate {


    @IBOutlet weak var Scroll: UIScrollView!
    
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.Scroll.maximumZoomScale = 5.0
        self.Scroll.minimumZoomScale = 0.5
        self.Scroll.delegate = self
        
     //   Scroll.drawRectTest(Scroll.frame)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return Scroll.superview
    }
    
}

